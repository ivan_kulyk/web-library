package beans;

import java.io.Serializable;
import java.util.Arrays;


public class Book implements Serializable, Identifier {

    private Long id;
    private String bookName;
    private byte[] content;
    private Integer pageCount;
    private Long idGenre;
    private Long idAuthor;
    private Long idPublisher;
    private Integer yearOfPublishing;
    private byte[] image;
    private String description;

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + bookName + '\'' +
                ", content=" + Arrays.toString(content) +
                ", pageCount=" + pageCount +
                ", idGenre=" + idGenre +
                ", idAuthor=" + idAuthor +
                ", idPublisher=" + idPublisher +
                ", yearOfPublishing=" + yearOfPublishing +
                ", image=" + Arrays.toString(image) +
                ", description='" + description + '\'' +
                '}' + '\n';
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public Long getIdGenre() {
        return idGenre;
    }

    public void setIdGenre(Long idGenre) {
        this.idGenre = idGenre;
    }

    public Long getIdAuthor() {
        return idAuthor;
    }

    public void setIdAuthor(Long idAuthor) {
        this.idAuthor = idAuthor;
    }

    public Long getIdPublisher() {
        return idPublisher;
    }

    public void setIdPublisher(Long idPublisher) {
        this.idPublisher = idPublisher;
    }

    public Integer getYearOfPublishing() {
        return yearOfPublishing;
    }

    public void setYearOfPublishing(Integer yearOfPublishing) {
        this.yearOfPublishing = yearOfPublishing;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

