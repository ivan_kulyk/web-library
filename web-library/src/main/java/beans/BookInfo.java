package beans;

import dao.FactoryDAO;
import exceptions.DaoException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class BookInfo {

    private Long id;
    private String bookName;
    private Integer pageCount;
    private Integer yearOfPublishing;
    private byte[] image;
    private byte[] content;


    private String firstName;
    private String lastName;
    private String middleName;

    private String publisherName;

    private String genreName;

    //private String contentQuery = "SELECT content FROM book WHERE id = " + this.getId();





    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public Integer getYearOfPublishing() {
        return yearOfPublishing;
    }

    public void setYearOfPublishing(Integer yearOfPublishing) {
        this.yearOfPublishing = yearOfPublishing;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public void fillPDF() {

        try (Connection connection = FactoryDAO.getInstance("db-config.properties").getConnection();
             Statement statement = connection.createStatement()){

            try(ResultSet resultSet = statement.executeQuery("SELECT content FROM book WHERE id = " + this.getId())) {

                while (resultSet.next()) {

                    this.setContent(resultSet.getBytes("content"));
                }

            }

        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }

    }

}
