package beans;

public interface Identifier {

    Long getId();
}
