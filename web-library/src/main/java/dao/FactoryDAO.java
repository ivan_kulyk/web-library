package dao;

import dao.impl.AuthorImplDAO;
import dao.impl.BookImplDAO;
import dao.impl.GenreImplDAO;
import dao.impl.PublisherImplDAO;
import dao.mapper.AuthorMapper;
import dao.mapper.BookMapper;
import dao.mapper.GenreMapper;
import dao.mapper.PublisherMapper;
import exceptions.DaoException;
import metadata.AuthorMetadata;
import metadata.BookMetadata;
import metadata.GenreMetadata;
import metadata.PublisherMetadata;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class FactoryDAO {

    private static FactoryDAO INSTANCE = null;

    private String url;
    private String username;
    private String password;

    private BookMetadata bookMetadata = null;
    private AuthorMetadata authorMetadata = null;
    private GenreMetadata genreMetadata = null;
    private PublisherMetadata publisherMetadata = null;

    private FactoryDAO(String configFile) {
        try {
            Properties daoProps = new Properties();
            daoProps.load(this.getClass().getClassLoader().getResourceAsStream(configFile));
            Class.forName(daoProps.getProperty("db.driver"));
            this.url = daoProps.getProperty("db.url");
            this.username = daoProps.getProperty("db.user");
            this.password = daoProps.getProperty("db.password");

        } catch (ClassNotFoundException | IOException e) {
            throw new DaoException(e.getMessage(), e);
        }
    }


    public static FactoryDAO getInstance(String configFile) {
        if (INSTANCE == null) {
            INSTANCE = new FactoryDAO(configFile);
        }
        return INSTANCE;
    }


    public Connection getConnection() {
        try {
            return DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
    }

    public AuthorMetadata getAuthorMetadata() {
        if (authorMetadata == null) {
            authorMetadata = new AuthorImplDAO(this, new AuthorMapper());
        }
        return authorMetadata;
    }

    public GenreMetadata getGenreMetadata() {
        if (genreMetadata == null) {
            genreMetadata = new GenreImplDAO(this, new GenreMapper());
        }
        return genreMetadata;
    }

    public PublisherMetadata getPublisherMetadata() {
        if (publisherMetadata == null) {
            publisherMetadata = new PublisherImplDAO(this, new PublisherMapper());
        }
        return publisherMetadata;
    }

    public BookMetadata getBookMetadata() {
        if (bookMetadata == null) {
            bookMetadata = new BookImplDAO(this, new BookMapper());
        }
        return bookMetadata;
    }
}
