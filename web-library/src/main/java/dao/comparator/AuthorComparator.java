package dao.comparator;

import entity.Author;

import java.util.Comparator;

public class AuthorComparator implements Comparator<Author> {
    @Override
    public int compare(Author o1, Author o2) {
        return o1.getLastName().compareTo(o2.getLastName());
    }
}
