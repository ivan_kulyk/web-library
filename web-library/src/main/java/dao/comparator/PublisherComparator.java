package dao.comparator;

import entity.Publisher;

import java.util.Comparator;

public class PublisherComparator implements Comparator<Publisher> {
    @Override
    public int compare(Publisher o1, Publisher o2) {
        return o1.getPublisherName().compareTo(o2.getPublisherName());
    }
}
