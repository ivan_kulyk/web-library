package dao.impl;

import entity.Identifier;
import dao.FactoryDAO;
import exceptions.DaoException;
import metadata.Metadata;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractDAO <T extends Identifier> implements Metadata<T> {

    protected final FactoryDAO factory;

    protected AbstractDAO(FactoryDAO factory) {
        this.factory = factory;
    }

    protected abstract String getSelectAllQuery();

    protected abstract String getSelectOneQuery();

    protected abstract String getInsertQuery();

    protected abstract String getUpdateQuery();

    protected abstract String getDeleteQuery();

    protected abstract T parseMetadata(ResultSet resultSet) throws SQLException;

    protected abstract void prepareInsertStatement(T metadata, PreparedStatement statement) throws SQLException;

    protected abstract void parseGeneratedKey(T metadata, ResultSet resultSet) throws SQLException;

    protected abstract void prepareUpdateStatement(T metadata, PreparedStatement statement) throws SQLException;


    @Override
    public Optional<T> getById(Long id) {
        String QUERY = getSelectOneQuery();

        try (Connection connection = factory.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(QUERY)) {

            preparedStatement.setLong(1, id);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                if (resultSet.next()) {
                    return Optional.of(parseMetadata(resultSet));
                }
            }

        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }

        return Optional.empty();
    }

    @Override
    public List<T> getAll() {
        List<T> listResult = new ArrayList<>();
        String query = getSelectAllQuery();

        try (Connection connection = factory.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query)) {


            while (resultSet.next()) {
                T metadata = parseMetadata(resultSet);
                listResult.add(metadata);
            }


        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }

        return listResult;
    }

    @Override
    public T create(T metadata) {

        String QUERY = getInsertQuery();

        try (Connection connection = factory.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(QUERY,
                     Statement.RETURN_GENERATED_KEYS)) {


            prepareInsertStatement(metadata, preparedStatement);

            preparedStatement.executeUpdate();
            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {

                if (resultSet.next()) {
                    parseGeneratedKey(metadata, resultSet);
                }
            }

        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
        return metadata;
    }

    @Override
    public Boolean update(T metadata) {

        String QUERY = getUpdateQuery();

        try (Connection connection = factory.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(QUERY)) {


            prepareUpdateStatement(metadata, preparedStatement);

            preparedStatement.executeUpdate();
            return preparedStatement.executeUpdate() == 1;

        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
    }

    @Override
    public Boolean delete(Long id) {

        String QUERY = getDeleteQuery();

        try (Connection connection = factory.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(QUERY)) {

                preparedStatement.setLong(1, id);
                return preparedStatement.executeUpdate() == 1;
            }
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
    }
}
