package dao.impl;

import entity.Author;
import dao.FactoryDAO;
import dao.mapper.Mapper;
import metadata.AuthorMetadata;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthorImplDAO extends AbstractDAO<Author> implements AuthorMetadata {

    private final Mapper<Author> mapper;

    public AuthorImplDAO(FactoryDAO factory, Mapper<Author> mapper) {
        super(factory);
        this.mapper = mapper;
    }

    @Override
    protected String getSelectAllQuery() {
        return "SELECT * FROM author";
    }

    @Override
    protected String getSelectOneQuery() {
        return "SELECT * FROM author WHERE id = ?";
    }

    @Override
    protected String getInsertQuery() {
        return "INSERT INTO author (first_name, last_name, middle_name, birthday, death_day) " +
                "VALUES (?, ?, ?, ?, ?)";
    }

    @Override
    protected String getUpdateQuery() {
        return "UPDATE author SET first_name = ?, last_name = ?, middle_name = ?," +
                " birthday = ?, death_day = ?" +
                " WHERE id = ?";
    }

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM author WHERE id = ?";
    }

    @Override
    protected Author parseMetadata(ResultSet resultSet) throws SQLException {
        return mapper.map(resultSet);
    }

    @Override
    protected void prepareInsertStatement(Author metadata, PreparedStatement statement) throws SQLException {

        statement.setString(1, metadata.getFirstName());
        statement.setString(2, metadata.getLastName());
        statement.setString(3, metadata.getMiddleName());
        statement.setDate(4, metadata.getBirthday());
        statement.setDate(5, metadata.getDeathDate());
    }

    @Override
    protected void parseGeneratedKey(Author metadata, ResultSet resultSet) throws SQLException {

        metadata.setId(resultSet.getLong(1));
    }

    @Override
    protected void prepareUpdateStatement(Author metadata, PreparedStatement statement) throws SQLException {

        prepareInsertStatement(metadata, statement);
        statement.setLong(6, metadata.getId());
    }
}
