package dao.impl;

import entity.Book;
import entity.BookInfo;
import dao.FactoryDAO;
import dao.mapper.BookMapper;
import exceptions.DaoException;
import metadata.BookMetadata;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookImplDAO extends AbstractDAO<Book> implements BookMetadata {

    private final BookMapper mapper;

    public BookImplDAO(FactoryDAO factory, BookMapper mapper) {
        super(factory);
        this.mapper = mapper;
    }

    @Override
    protected String getSelectAllQuery() {
        return "SELECT * FROM book";
    }

    @Override
    protected String getSelectOneQuery() {
        return "SELECT * FROM book WHERE id = ?";
    }

    @Override
    protected String getInsertQuery() {
        return "INSERT INTO book (book_name, content, page_count, id_genre, id_author, id_publisher," +
                "year_of_publishing, image, description) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    }

    @Override
    protected String getUpdateQuery() {
        return "UPDATE book SET book_name = ?, content = ?, page_count = ?, id_genre = ?, id_author = ?," +
                " id_publisher = ?, year_of_publishing = ?, image = ?, description = ?" +
                " WHERE id = ?";
    }

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM book WHERE id = ?";
    }

    @Override
    protected Book parseMetadata(ResultSet resultSet) throws SQLException {
        return mapper.map(resultSet);
    }

    @Override
    protected void prepareInsertStatement(Book metadata, PreparedStatement statement) throws SQLException {

        statement.setString(1, metadata.getBookName());
        statement.setBytes(2, metadata.getContent());
        statement.setInt(3, metadata.getPageCount());
        statement.setLong(4, metadata.getIdGenre());
        statement.setLong(5, metadata.getIdAuthor());
        statement.setLong(6, metadata.getIdPublisher());
        statement.setInt(7, metadata.getYearOfPublishing());
        statement.setBytes(8, metadata.getImage());
        statement.setString(9, metadata.getDescription());
    }

    @Override
    protected void parseGeneratedKey(Book metadata, ResultSet resultSet) throws SQLException {

        metadata.setId(resultSet.getLong(1));
    }

    @Override
    protected void prepareUpdateStatement(Book metadata, PreparedStatement statement) throws SQLException {

        prepareInsertStatement(metadata, statement);
        statement.setLong(10, metadata.getId());
    }

    @Override
    public List<BookInfo> getAllBooksInfo(Long id) throws SQLException {
        List<BookInfo> bookList = new ArrayList<>();

        String QUERY = "SELECT book.id, book.book_name, book.page_count, book.image, book.year_of_publishing,\n" +
                "       p.publisher_name,\n" +
                "       a.first_name,\n" +
                "       a.middle_name,\n" +
                "       a.last_name,\n" +
                "       g.genre_name\n" +
                "       FROM book\n" +
                "       INNER JOIN publisher p  ON book.id_publisher = p.id\n" +
                "       INNER JOIN author a ON book.id_author = a.id\n" +
                "       INNER JOIN genre g ON book.id_genre = g.id WHERE id_genre = ?";


        //Statement statement = FactoryDAO.getInstance("db-config.properties").getConnection().createStatement();



        try (Connection connection = factory.getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY)){

            statement.setLong(1, id);
            try(ResultSet resultSet = statement.executeQuery()) {

                while (resultSet.next()) {

                    BookInfo bookInfo = new BookInfo();

                    bookInfo.setId(resultSet.getLong("id"));
                    bookInfo.setBookName(resultSet.getString("book_name"));
                    bookInfo.setPageCount(resultSet.getInt("page_count"));
                    bookInfo.setYearOfPublishing(resultSet.getInt("year_of_publishing"));
                    bookInfo.setImage(resultSet.getBytes("image"));

                    bookInfo.setPublisherName(resultSet.getString("publisher_name"));

                    bookInfo.setFirstName(resultSet.getString("first_name"));
                    bookInfo.setMiddleName(resultSet.getString("middle_name"));
                    bookInfo.setLastName(resultSet.getString("last_name"));

                    bookInfo.setGenreName(resultSet.getString("genre_name"));

                    bookList.add(bookInfo);
                }

            }

        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }

        return bookList;
    }

}
