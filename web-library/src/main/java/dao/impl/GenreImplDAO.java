package dao.impl;

import entity.Genre;
import dao.FactoryDAO;
import dao.mapper.GenreMapper;
import metadata.GenreMetadata;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GenreImplDAO extends AbstractDAO<Genre> implements GenreMetadata {

    private final GenreMapper mapper;

    public GenreImplDAO(FactoryDAO factory, GenreMapper mapper) {
        super(factory);
        this.mapper = mapper;
    }

    @Override
    protected String getSelectAllQuery() {
        return "SELECT * FROM genre";
    }

    @Override
    protected String getSelectOneQuery() {
        return "SELECT * FROM genre WHERE id = ?";
    }

    @Override
    protected String getInsertQuery() {
        return "INSERT INTO genre (genre_name) VALUES (?)";
    }

    @Override
    protected String getUpdateQuery() {
        return "UPDATE genre SET genre_name = ? WHERE id = ?";
    }

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM genre WHERE id = ?";
    }

    @Override
    protected Genre parseMetadata(ResultSet resultSet) throws SQLException {
        return mapper.map(resultSet);
    }

    @Override
    protected void prepareInsertStatement(Genre metadata, PreparedStatement statement) throws SQLException {

        statement.setString(1, metadata.getGenreName());
    }

    @Override
    protected void parseGeneratedKey(Genre metadata, ResultSet resultSet) throws SQLException {

        metadata.setId(resultSet.getLong(1));
    }

    @Override
    protected void prepareUpdateStatement(Genre metadata, PreparedStatement statement) throws SQLException {

        prepareInsertStatement(metadata, statement);
        statement.setLong(2, metadata.getId());
    }
}
