package dao.impl;

import entity.Publisher;
import dao.FactoryDAO;
import dao.mapper.PublisherMapper;
import metadata.PublisherMetadata;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PublisherImplDAO extends AbstractDAO<Publisher> implements PublisherMetadata {

    private final PublisherMapper mapper;

    public PublisherImplDAO(FactoryDAO factory, PublisherMapper mapper) {
        super(factory);
        this.mapper = mapper;
    }

    @Override
    protected String getSelectAllQuery() {
        return "SELECT * FROM publisher";
    }

    @Override
    protected String getSelectOneQuery() {
        return "SELECT * FROM publisher WHERE id = ?";
    }

    @Override
    protected String getInsertQuery() {
        return "INSERT INTO publisher (publisher_name) VALUES (?)";
    }

    @Override
    protected String getUpdateQuery() {
        return "UPDATE publisher SET publisher_name = ? WHERE id = ?";
    }

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM publisher WHERE id = ?";
    }

    @Override
    protected Publisher parseMetadata(ResultSet resultSet) throws SQLException {
        return mapper.map(resultSet);
    }

    @Override
    protected void prepareInsertStatement(Publisher metadata, PreparedStatement statement) throws SQLException {

        statement.setString(1, metadata.getPublisherName());
    }

    @Override
    protected void parseGeneratedKey(Publisher metadata, ResultSet resultSet) throws SQLException {

        metadata.setId(resultSet.getLong(1));
    }

    @Override
    protected void prepareUpdateStatement(Publisher metadata, PreparedStatement statement) throws SQLException {

        prepareInsertStatement(metadata, statement);
        statement.setLong(2, metadata.getId());
    }
}
