package dao.mapper;

import entity.Author;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthorMapper implements Mapper<Author> {
    @Override
    public Author map(ResultSet resultSet) throws SQLException {

        Author author = new Author();

        author.setId(resultSet.getLong("id"));
        author.setFirstName(resultSet.getString("first_name"));
        author.setLastName(resultSet.getString("last_name"));
        author.setMiddleName(resultSet.getString("middle_name"));
        author.setBirthday(resultSet.getDate("birthday"));
        author.setDeathDate(resultSet.getDate("death_day"));

        return author;
    }
}
