package dao.mapper;

import entity.Book;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BookMapper implements Mapper<Book> {

    @Override
    public Book map(ResultSet resultSet) throws SQLException {

        Book book = new Book();

        book.setId(resultSet.getLong("id"));
        book.setBookName(resultSet.getString("book_name"));
        book.setContent(resultSet.getBytes("content"));
        book.setPageCount(resultSet.getInt("page_count"));
        book.setIdGenre(resultSet.getLong("id_genre"));
        book.setIdAuthor(resultSet.getLong("id_author"));
        book.setIdPublisher(resultSet.getLong("id_publisher"));
        book.setYearOfPublishing(resultSet.getInt("year_of_publishing"));
        book.setImage(resultSet.getBytes("image"));
        book.setDescription(resultSet.getString("description"));

        return book;
    }
}
