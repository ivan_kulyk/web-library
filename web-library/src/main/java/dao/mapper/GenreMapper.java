package dao.mapper;

import entity.Genre;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GenreMapper implements Mapper<Genre> {

    @Override
    public Genre map(ResultSet resultSet) throws SQLException {

        Genre genre = new Genre();
        genre.setId(resultSet.getLong("id"));
        genre.setGenreName(resultSet.getString("genre_name"));
        return genre;
    }
}
