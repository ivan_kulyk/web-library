package dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface Mapper<M> {

    M map(ResultSet resultSet) throws SQLException;
}
