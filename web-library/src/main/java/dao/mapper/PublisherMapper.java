package dao.mapper;

import entity.Publisher;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PublisherMapper implements Mapper<Publisher> {

    @Override
    public Publisher map(ResultSet resultSet) throws SQLException {

        Publisher publisher = new Publisher();
        publisher.setId(resultSet.getLong("id"));
        publisher.setPublisherName(resultSet.getString("publisher_name"));
        return publisher;
    }
}