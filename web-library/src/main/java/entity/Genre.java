package entity;

import java.io.Serializable;

public class Genre implements Serializable, Identifier {

    private Long id;
    private String genreName;

    @Override
    public String toString() {
        return "Genre{" +
                "id=" + id +
                ", name='" + genreName + '\'' +
                '}';
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }
}
