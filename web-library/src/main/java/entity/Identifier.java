package entity;

public interface Identifier {

    Long getId();
}
