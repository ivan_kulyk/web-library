package entity;

import java.io.Serializable;

public class Publisher implements Serializable, Identifier {

    private Long id;
    private String publisherName;

    @Override
    public String toString() {
        return "Publisher{" +
                "id=" + id +
                ", name='" + publisherName + '\'' +
                '}';
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }
}
