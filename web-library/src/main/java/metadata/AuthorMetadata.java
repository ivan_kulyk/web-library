package metadata;

import entity.Author;

public interface AuthorMetadata extends Metadata<Author> {
}
