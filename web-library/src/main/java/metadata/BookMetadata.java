package metadata;

import entity.Book;
import entity.BookInfo;

import java.sql.SQLException;
import java.util.List;

public interface BookMetadata extends Metadata<Book> {

    List<BookInfo> getAllBooksInfo(Long idGenre) throws SQLException;

    //void fillPDF();
}
