package metadata;

import entity.Genre;

public interface GenreMetadata extends Metadata<Genre> {
}
