package metadata;

import entity.Identifier;

import java.util.List;
import java.util.Optional;

public interface Metadata<T extends Identifier> {

    Optional<T> getById(Long id);

    List<T> getAll();

    T create(T metadata);

    Boolean update(T metadata);

    Boolean delete(Long id);
}
