package metadata;

import entity.Publisher;

public interface PublisherMetadata extends Metadata<Publisher> {
}
