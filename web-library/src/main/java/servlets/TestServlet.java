package servlets;

import beans.BookInfo;
import dao.FactoryDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.SQLException;
import java.util.List;

public class TestServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            performTask(req, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            performTask(req, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private void performTask(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException, SQLException {

        List<BookInfo> allBooksInfo = FactoryDAO.getInstance("db-config.properties").getBookMetadata().getAllBooksInfo(1L);
        PrintWriter out = response.getWriter();
        String pdfFileName = "rum dairy.pdf";
        String filePath = "D:\\WEB\\DB\\";



        response.setContentType("application/pdf");
        response.addHeader("Content-Disposition", "inline; pdfFileName=\\" + pdfFileName);


        FileInputStream fileInputStream = new FileInputStream(filePath+pdfFileName);
        int bytes;
        while ((bytes = fileInputStream.read()) != -1) {
            out.write(bytes);
        }
        fileInputStream.close();
        out.close();

    }

}
