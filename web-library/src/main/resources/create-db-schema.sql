USE library;

CREATE TABLE IF NOT EXISTS book (
   id                     BIGINT PRIMARY KEY AUTO_INCREMENT,
   book_name              VARCHAR(80)    NOT NULL,
   content                LONGBLOB       NOT NULL,
   page_count             INT            NOT NULL,
   id_genre               BIGINT         NOT NULL,
   id_author              BIGINT         NOT NULL,
   id_publisher           BIGINT         NOT NULL,
   year_of_publishing     INT            NOT NULL,
   image                  LONGBLOB       NOT NULL,
   description            VARCHAR(3000)  NOT NULL
);

CREATE TABLE IF NOT EXISTS publisher (
   id                     BIGINT PRIMARY KEY AUTO_INCREMENT,
   publisher_name         VARCHAR(80)    NOT NULL
);

CREATE TABLE IF NOT EXISTS author (
   id                     BIGINT PRIMARY KEY AUTO_INCREMENT,
   first_name             VARCHAR(80)    NOT NULL,
   last_name              VARCHAR(80)    NOT NULL,
   middle_name            VARCHAR(80),
   birthday               DATE           NOT NULL,
   death_day              DATE
);

CREATE TABLE IF NOT EXISTS genre (
   id                     BIGINT PRIMARY KEY AUTO_INCREMENT,
   genre_name                   VARCHAR(80)    NOT NULL
);

ALTER TABLE book
   ADD CONSTRAINT FK_book_author
 FOREIGN KEY (id_author)
 REFERENCES author(id);

 ALTER TABLE book
   ADD CONSTRAINT FK_book_genre
 FOREIGN KEY (id_genre)
 REFERENCES genre(id);

 ALTER TABLE book
   ADD CONSTRAINT FK_book_publisher
 FOREIGN KEY (id_publisher)
 REFERENCES publisher(id);
