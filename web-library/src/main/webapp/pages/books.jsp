<%@ page import="entity.Book" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="dao.FactoryDAO" %>
<%@ page import="java.util.List" %>
<%@ page import="entity.BookInfo" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@include file="../WEB-INF/jspf/left_menu.jspf" %>

<%request.setCharacterEncoding("UTF-8");

    long idGenre = 0L;

    try {
        idGenre = Long.valueOf(request.getParameter("id_genre"));

    } catch (Exception ex) {
        ex.printStackTrace();
    }

%>

<%--<jsp:useBean id="bookList" class="dao.impl." scope="page"/>--%>



<div class="book_list">
    <h3>${param.name}</h3>



    <%
        List<BookInfo> allBooksInfo = FactoryDAO.getInstance("db-config.properties").getBookMetadata().getAllBooksInfo(idGenre);

        session.setAttribute("currentBookList", allBooksInfo);
        for (BookInfo book : allBooksInfo) {

    %>

    <div class="book_info">
        <div class="book_title">
            <p> <%=book.getBookName()%></p>
        </div>
        <div class="book_image">
            <img src="<%=request.getContextPath()%>/ShowImage?index=<%=allBooksInfo.indexOf(book) %>" height="250" width="190" alt="Обложка"/>
        </div>
        <div class="book_details">

            <br><strong>Автор:</strong> <%=book.getFirstName() + " " + book.getLastName() %>
            <br><strong>Кількість сторінок:</strong> <%=book.getPageCount() %>
            <br><strong>Видавництво:</strong> <%=book.getPublisherName() %>
            <br><strong>Рік видання:</strong> <%=book.getYearOfPublishing() %>


            <p style="margin:10px;"> <a href="<%=request.getContextPath()%>/PdfContent?index=<%=allBooksInfo.indexOf(book)%>" target="_blank"﻿>Читать</a></p>
        </div>
    </div>

    <%}%>
</div>
