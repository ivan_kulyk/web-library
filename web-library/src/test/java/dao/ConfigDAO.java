package dao;

import org.h2.tools.RunScript;
import org.junit.After;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;

public abstract class ConfigDAO {

    private FactoryDAO factory = FactoryDAO.getInstance("db-config-test.properties");

    protected FactoryDAO getFactory() {
        return factory;
    }

    protected void execScripts(String... scripts) throws SQLException {
        for (String script : scripts) {
            InputStream scriptStream = this.getClass().getClassLoader().getResourceAsStream(script);
            RunScript.execute(getFactory().getConnection(), new InputStreamReader(scriptStream));
        }
    }

    protected void initDb(String... scripts) throws SQLException {
        execScripts("sql/create-db.sql");
        execScripts(scripts);
    }

    @After
    public void tearDown() throws Exception {
        InputStream script = this.getClass().getClassLoader().getResourceAsStream("sql/drop-db.sql");
        RunScript.execute(getFactory().getConnection(), new InputStreamReader(script));
    }
}
