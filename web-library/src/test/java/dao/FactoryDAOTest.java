package dao;

import org.junit.Test;

import static org.junit.Assert.assertSame;

public class FactoryDAOTest {

    @Test
    public void shouldReturnSingleInstance() {
        //given
        String configFile = "db-config-test.properties";
        FactoryDAO expectedFactory = FactoryDAO.getInstance(configFile);

        //when
        FactoryDAO actualFactory = FactoryDAO.getInstance(configFile);

        //then
        assertSame("DaoFactory must be a singleton", expectedFactory, actualFactory);
    }
}