package dao.impl;

import entity.Author;
import dao.ConfigDAO;
import metadata.AuthorMetadata;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class AuthorImplDAOTest extends ConfigDAO {

    private AuthorMetadata dao = getFactory().getAuthorMetadata();

    @Before
    public void setUp() throws Exception {
        initDb();
    }

    @Test
    public void shouldReturnListOfAuthors() throws SQLException {
        //given
        execScripts("sql/author-insert.sql");

        //when
        List<Author> authorList = dao.getAll();

        //then
        assertThat(authorList).hasSize(2);
        Author author = authorList.get(0);
        assertThat(author.getId()).isNotNull();
        assertThat(author.getFirstName()).isEqualTo("Лев");
        assertThat(author.getLastName()).isEqualTo("Толстой");
        assertThat(author.getMiddleName()).isEqualTo("Миколайович");
        assertThat(author.getBirthday()).isEqualTo("1828-09-09");
        assertThat(author.getDeathDate()).isEqualTo("1910-10-20");
    }

    @Test
    public void shouldReturnAuthorById() throws SQLException {
        //given
        execScripts("sql/author-insert.sql");

        //when
        Optional<Author> maydeAuthor = dao.getById(2L);

        //then
        Author author = maydeAuthor.orElse(null);
        assertThat(author).isNotNull();
        assertThat(author.getId()).isNotNull();
        assertThat(author.getFirstName()).isEqualTo("Дмитро");
        assertThat(author.getLastName()).isEqualTo("Глуховськй");
        assertThat(author.getMiddleName()).isEqualTo("Олексійович");
        assertThat(author.getBirthday()).isEqualTo(Date.valueOf("1979-06-12"));
        assertThat(author.getDeathDate()).isEqualTo((Date) null);
    }

    @Test
    public void shouldCreateParticipantAndUpdateId() {
        //given
        Author author = new Author();
        author.setFirstName("Джек");
        author.setLastName("Керуак");
        author.setMiddleName("");
        author.setBirthday(Date.valueOf("1922-03-12"));
        author.setDeathDate(Date.valueOf("1969-10-21"));

        //when
        Author persistedAuthor = dao.create(author);

        //then
        assertThat(persistedAuthor).isSameAs(author);
        assertThat(persistedAuthor.getId()).isNotNull();
    }

    @Test
    public void shouldUpdateAuthor() throws SQLException {
        //given
        execScripts("sql/author-insert.sql");
        Author author = new Author();
        author.setId(2L);
        author.setFirstName("Дмитро");
        author.setLastName("Глуховськй");
        author.setMiddleName("");
        author.setBirthday(Date.valueOf("1979-06-12"));
        author.setDeathDate(null);

        //when
        Boolean isUpdated = dao.update(author);

        //then
        assertThat(isUpdated).isTrue();
    }

    @Test
    public void shouldDeleteAuthorById() throws SQLException {
        //given
        execScripts("sql/author-insert.sql");

        //when
        Boolean isDeleted = dao.delete(1L);

        //then
        assertThat(isDeleted).isTrue();
    }

}