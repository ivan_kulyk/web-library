package dao.impl;

import entity.Book;
import dao.ConfigDAO;
import metadata.BookMetadata;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class BookImplDAOTest extends ConfigDAO {

    private BookMetadata dao = getFactory().getBookMetadata();

    @Before
    public void setUp() throws Exception {
        initDb( "sql/author-insert.sql",
                "sql/genre-insert.sql", "sql/publisher-insert.sql");
    }

    @Test
    public void shouldReturnListOfBookss() throws SQLException, IOException {
        //given
        execScripts("sql/book-insert.sql");

        File content = new File("src\\test\\resources\\text\\text1.txt");
        byte[] fileContent = Files.readAllBytes(content.toPath());
        File image = new File("src\\test\\resources\\image\\pic1.png");
        byte[] fileImage = Files.readAllBytes(image.toPath());

        //when
        List<Book> authorList = dao.getAll();

        //then
        assertThat(authorList).hasSize(2);
        Book book = authorList.get(0);
        assertThat(book.getId()).isNotNull();
        assertThat(book.getBookName()).isEqualTo("Текст");
        assertThat(book.getContent()).isEqualTo(fileContent);
        assertThat(book.getPageCount()).isEqualTo(345);
        assertThat(book.getIdGenre()).isEqualTo(2L);
        assertThat(book.getIdAuthor()).isEqualTo(2L);
        assertThat(book.getIdPublisher()).isEqualTo(2L);
        assertThat(book.getYearOfPublishing()).isEqualTo(2017);
        assertThat(book.getImage()).isEqualTo(fileImage);
        assertThat(book.getDescription()).isEqualTo("Текст Глуховский");

    }

    @Test
    public void shouldReturnBookById() throws SQLException, IOException {
        //given
        execScripts("sql/book-insert.sql");
        File content = new File("src\\test\\resources\\text\\text2.txt");
        byte[] fileContent = Files.readAllBytes(content.toPath());
        File image = new File("src\\test\\resources\\image\\pic2.png");
        byte[] fileImage = Files.readAllBytes(image.toPath());

        //when
        Optional<Book> maydeBook = dao.getById(2L);

        //then
        Book book = maydeBook.orElse(null);
        assertThat(book).isNotNull();
        assertThat(book.getBookName()).isEqualTo("Метро 2033");
        assertThat(book.getContent()).isEqualTo(fileContent);
        assertThat(book.getPageCount()).isEqualTo(289);
        assertThat(book.getIdGenre()).isEqualTo(2L);
        assertThat(book.getIdAuthor()).isEqualTo(2L);
        assertThat(book.getIdPublisher()).isEqualTo(2L);
        assertThat(book.getYearOfPublishing()).isEqualTo(2007);
        assertThat(book.getImage()).isEqualTo(fileImage);
        assertThat(book.getDescription()).isEqualTo("Метро 2033 Глуховский");
    }

    @Test
    public void shouldCreateBookAndUpdateId() throws IOException {
        //given
        File content = new File("src\\test\\resources\\text\\text3.txt");
        byte[] fileContent = Files.readAllBytes(content.toPath());
        File image = new File("src\\test\\resources\\image\\pic3.png");
        byte[] fileImage = Files.readAllBytes(image.toPath());

        Book book = new Book();
        book.setBookName("Сумерки");
        book.setContent(fileContent);
        book.setPageCount(300);
        book.setIdGenre(2L);
        book.setIdAuthor(2L);
        book.setIdPublisher(3L);
        book.setYearOfPublishing(2009);
        book.setImage(fileImage);
        book.setDescription("Сумерки Глуховского");

        //when
        Book persistedBook = dao.create(book);

        //then
        assertThat(persistedBook).isSameAs(book);
        assertThat(persistedBook.getId()).isNotNull();
    }

    @Test
    public void shouldUpdateBook() throws SQLException, IOException {
        //given
        execScripts("sql/book-insert.sql");

        File content = new File("src\\test\\resources\\text\\text3.txt");
        byte[] fileContent = Files.readAllBytes(content.toPath());
        File image = new File("src\\test\\resources\\image\\pic3.png");
        byte[] fileImage = Files.readAllBytes(image.toPath());


        Book book = new Book();
        book.setId(2L);
        book.setBookName("Сумерки");
        book.setContent(fileContent);
        book.setPageCount(300);
        book.setIdGenre(2L);
        book.setIdAuthor(2L);
        book.setIdPublisher(3L);
        book.setYearOfPublishing(2009);
        book.setImage(fileImage);
        book.setDescription("Сумерки Глуховского");

        //when
        Boolean isUpdated = dao.update(book);

        //then
        assertThat(isUpdated).isTrue();
    }

    @Test
    public void shouldDeleteAuthorById() throws SQLException {
        //given
        execScripts("sql/book-insert.sql");

        //when
        Boolean isDeleted = dao.delete(1L);

        //then
        assertThat(isDeleted).isTrue();
    }

}