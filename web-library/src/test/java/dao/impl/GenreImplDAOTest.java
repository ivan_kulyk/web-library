package dao.impl;

import entity.Genre;
import dao.ConfigDAO;
import metadata.GenreMetadata;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class GenreImplDAOTest extends ConfigDAO {

    private GenreMetadata dao = getFactory().getGenreMetadata();

    @Before
    public void setUp() throws Exception {
        initDb();
    }

    @Test
    public void shouldReturnListOfGeners() throws SQLException {
        //given
        execScripts("sql/genre-insert.sql");

        //when
        List<Genre> genreList = dao.getAll();

        //then
        assertThat(genreList).hasSize(4);
        Genre genre = genreList.get(2);
        assertThat(genre.getId()).isNotNull();
        assertThat(genre.getGenreName()).isEqualTo("Драма");
    }

    @Test
    public void shouldReturnGenreById() throws SQLException {
        //given
        execScripts("sql/genre-insert.sql");

        //when
        Optional<Genre> maydeGenre = dao.getById(2L);

        //then
        Genre genre = maydeGenre.orElse(null);
        assertThat(genre).isNotNull();
        assertThat(genre.getId()).isNotNull();
        assertThat(genre.getGenreName()).isEqualTo("Роман");

    }

    @Test
    public void shouldCreateGenreAndUpdateId() {
        //given
        Genre genre = new Genre();
        genre.setGenreName("Опера");


        //when
        Genre persistedGenre = dao.create(genre);

        //then
        assertThat(persistedGenre).isSameAs(genre);
        assertThat(persistedGenre.getId()).isNotNull();
    }

    @Test
    public void shouldUpdateGenre() throws SQLException {
        //given
        execScripts("sql/genre-insert.sql");
        Genre genre = new Genre();
        genre.setId(2L);
        genre.setGenreName("Жахи");


        //when
        Boolean isUpdated = dao.update(genre);

        //then
        assertThat(isUpdated).isTrue();
    }

    @Test
    public void shouldDeleteGenreById() throws SQLException {
        //given
        execScripts("sql/genre-insert.sql");

        //when
        Boolean isDeleted = dao.delete(1L);

        //then
        assertThat(isDeleted).isTrue();
    }

}