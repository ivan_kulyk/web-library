package dao.impl;

import entity.Publisher;
import dao.ConfigDAO;
import metadata.PublisherMetadata;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class PublisherImplDAOTest extends ConfigDAO {

    private PublisherMetadata dao = getFactory().getPublisherMetadata();

    @Before
    public void setUp() throws Exception {
        initDb();
    }

    @Test
    public void shouldReturnListOfPublishers() throws SQLException {
        //given
        execScripts("sql/publisher-insert.sql");

        //when
        List<Publisher> publisherList = dao.getAll();

        //then
        assertThat(publisherList).hasSize(3);
        Publisher genre = publisherList.get(2);
        assertThat(genre.getId()).isNotNull();
        assertThat(genre.getPublisherName()).isEqualTo("РИКСА");
    }

    @Test
    public void shouldReturnGenreById() throws SQLException {
        //given
        execScripts("sql/publisher-insert.sql");

        //when
        Optional<Publisher> maydePublisher = dao.getById(2L);

        //then
        Publisher genre = maydePublisher.orElse(null);
        assertThat(genre).isNotNull();
        assertThat(genre.getId()).isNotNull();
        assertThat(genre.getPublisherName()).isEqualTo("ЛАЛАРО");

    }

    @Test
    public void shouldCreateGenreAndUpdateId() {
        //given
        Publisher genre = new Publisher();
        genre.setPublisherName("Опера");


        //when
        Publisher persistedPublisher = dao.create(genre);

        //then
        assertThat(persistedPublisher).isSameAs(genre);
        assertThat(persistedPublisher.getId()).isNotNull();
    }

    @Test
    public void shouldUpdatePublisher() throws SQLException {
        //given
        execScripts("sql/publisher-insert.sql");
        Publisher genre = new Publisher();
        genre.setId(2L);
        genre.setPublisherName("Жахи");


        //when
        Boolean isUpdated = dao.update(genre);

        //then
        assertThat(isUpdated).isTrue();
    }

    @Test
    public void shouldDeletePublisherById() throws SQLException {
        //given
        execScripts("sql/publisher-insert.sql");

        //when
        Boolean isDeleted = dao.delete(1L);

        //then
        assertThat(isDeleted).isTrue();
    }

}