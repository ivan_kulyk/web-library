package dao.mapper;

import entity.Author;
import org.junit.Test;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AuthorMapperTest {

    @Test
    public void shouldReturnFullAuthorMetadataFromResultSet() throws SQLException {
        //given
        Mapper<Author> mapper = new AuthorMapper();

        ResultSet mockResultSet = mock(ResultSet.class);
        when(mockResultSet.getLong("id")).thenReturn(1L);
        when(mockResultSet.getString("first_name")).thenReturn("Лев");

        //when
        Author author = mapper.map(mockResultSet);

        //than
        assertThat(author.getId()).isEqualTo(1L);
        assertThat(author.getFirstName()).isEqualTo("Лев");
    }
}