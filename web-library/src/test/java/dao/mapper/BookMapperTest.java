package dao.mapper;

import entity.Book;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BookMapperTest {

    @Test
    public void shouldReturnFullBookMetadataFromResultSet() throws SQLException {
        //given
        Mapper<Book> mapper = new BookMapper();

        byte[] content = {1, 2, 3, 9};
        byte[] image = {12, 33, 99};

        ResultSet mockResultSet = mock(ResultSet.class);
        when(mockResultSet.getLong("id")).thenReturn(1L);
        when(mockResultSet.getString("book_name")).thenReturn("Книга");
        when(mockResultSet.getBytes("content")).thenReturn(content);
        when(mockResultSet.getInt("page_count")).thenReturn(129);
        when(mockResultSet.getLong("id_genre")).thenReturn(1L);
        when(mockResultSet.getLong("id_author")).thenReturn(2L);
        when(mockResultSet.getLong("id_publisher")).thenReturn(3L);
        when(mockResultSet.getInt("year_of_publishing")).thenReturn(1234);
        when(mockResultSet.getBytes("image")).thenReturn(image);
        when(mockResultSet.getString("description")).thenReturn("Гарна книга");

        //when
        Book book = mapper.map(mockResultSet);

        //than
        assertThat(book.getId()).isEqualTo(1L);
        assertThat(book.getBookName()).isEqualTo("Книга");
        assertThat(book.getContent()).isEqualTo(content);
        assertThat(book.getPageCount()).isEqualTo(129);
        assertThat(book.getIdGenre()).isEqualTo(1L);
        assertThat(book.getIdAuthor()).isEqualTo(2L);
        assertThat(book.getIdPublisher()).isEqualTo(3L);
        assertThat(book.getYearOfPublishing()).isEqualTo(1234);
        assertThat(book.getImage()).isEqualTo(image);
        assertThat(book.getDescription()).isEqualTo("Гарна книга");
    }

}