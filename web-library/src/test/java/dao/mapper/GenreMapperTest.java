package dao.mapper;

import entity.Genre;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GenreMapperTest {

    @Test
    public void shouldReturnFullGenreMetadataFromResultSet() throws SQLException {
        //given
        Mapper<Genre> mapper = new GenreMapper();

        ResultSet mockResultSet = mock(ResultSet.class);
        when(mockResultSet.getLong("id")).thenReturn(1L);
        when(mockResultSet.getString("genre_name")).thenReturn("ЭСТО");

        //when
        Genre author = mapper.map(mockResultSet);

        //than
        assertThat(author.getId()).isEqualTo(1L);
        assertThat(author.getGenreName()).isEqualTo("ЭСТО");

    }

}