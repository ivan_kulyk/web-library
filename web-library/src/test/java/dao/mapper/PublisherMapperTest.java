package dao.mapper;

import entity.Publisher;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PublisherMapperTest {

    @Test
    public void shouldReturnFullPublisherMetadataFromResultSet() throws SQLException {
        //given
        Mapper<Publisher> mapper = new PublisherMapper();

        ResultSet mockResultSet = mock(ResultSet.class);
        when(mockResultSet.getLong("id")).thenReturn(1L);
        when(mockResultSet.getString("publisher_name")).thenReturn("ЭСТО");

        //when
        Publisher publisher = mapper.map(mockResultSet);

        //than
        assertThat(publisher.getId()).isEqualTo(1L);
        assertThat(publisher.getPublisherName()).isEqualTo("ЭСТО");
    }


}